package actuator

import (
	"encoding/json"
	"gitlab.com/alexrockshouts/actuate/internal/prom"
	"net/http"
	"strings"

	"gitlab.com/alexrockshouts/actuate/internal"
	"gitlab.com/alexrockshouts/actuate/internal/env"
	"gitlab.com/alexrockshouts/actuate/internal/metrics"
	"gitlab.com/alexrockshouts/actuate/internal/threaddump"
	"gitlab.com/alexrockshouts/actuate/pkg/health"
	"gitlab.com/alexrockshouts/actuate/pkg/info"
)

type Endpoint string

const (
	INFO             Endpoint = "info"
	ENV              Endpoint = "env"
	THREADDUMP       Endpoint = "threaddump"
	SHUTDOWN         Endpoint = "shutdown"
	METRICS          Endpoint = "metrics"
	HEALTH           Endpoint = "health"
	HEALTH_READINESS Endpoint = "readiness"
	HEALTH_LIVENESS  Endpoint = "liveness"
	PROMETHEUS       Endpoint = "prometheus"
)

// All endpoints supported by the actuator
var allEndpoints = []Endpoint{
	INFO,
	ENV,
	THREADDUMP,
	METRICS,
	HEALTH,
	SHUTDOWN,
	PROMETHEUS,
}

type Config struct {
	Endpoints      []Endpoint
	HealthCheckers []health.HealthChecker
	ContextPath    string
}

// setConfigDefaults - Set the default values if fields in the config are empty
func setConfigDefaults(config *Config) {
	// If the endpoints are null or empty, set them to the defaults
	if config.Endpoints == nil {
		config.Endpoints = allEndpoints
	}

	if config.HealthCheckers == nil {
		config.HealthCheckers = health.DefaultHealthCheckers
	}
}

// GetHandler - Prepares the handler function to be attached to an endpoint
func GetHandler(config *Config) http.HandlerFunc {
	setConfigDefaults(config)

	// Add all the health checkers set by the configuration
	for _, checker := range config.HealthCheckers {
		health.Add(checker)
	}

	// Prepare the list of endpoints based on what is configured
	handlerMap := make(map[Endpoint]http.HandlerFunc)
	for _, ep := range config.Endpoints {
		switch ep {
		case INFO:

			handlerMap[INFO] = info.HandleInfo
		case ENV:

			handlerMap[ENV] = env.HandleEnv
		case METRICS:

			handlerMap[METRICS] = metrics.HandleMetrics
		case HEALTH:

			handlerMap[HEALTH] = health.HandleHealthchecks
			handlerMap[HEALTH_READINESS] = health.HandleReadiness
			handlerMap[HEALTH_LIVENESS] = health.HandleLiveliness

		case THREADDUMP:

			handlerMap[THREADDUMP] = threaddump.HandleThreaddump
		case SHUTDOWN:
			handlerMap[SHUTDOWN] = internal.HandleShudown

		case PROMETHEUS:
			handlerMap[PROMETHEUS] = prom.HandlePrometheus
		}
	}

	return func(writer http.ResponseWriter, request *http.Request) {
		// Validate the request. We only accept GET protocol here
		if request.Method != "GET" {
			writer.WriteHeader(http.StatusMethodNotAllowed)
			_, _ = writer.Write([]byte("Method Not Allowed"))
			return
		}

		// Extract the path from the URL
		paths := strings.Split(request.URL.Path, "/")
		entrypoint := ""
		pathLength := len(paths)
		if pathLength > 0 {
			entrypoint = paths[pathLength-1]
		}

		// Assumption: if we have an empty string, assume we're requesting the root url
		if len(entrypoint) == 0 {
			links := make([]string, 0)
			for k := range handlerMap {
				links = append(links, config.ContextPath+"/"+string(k))
			}

			payload := map[string][]string{"_links": links}
			_ = json.NewEncoder(writer).Encode(payload)
			return
		}

		// Match the handler with the appropriate endpoint
		if handler, ok := handlerMap[Endpoint(entrypoint)]; ok {
			handler(writer, request)
			return
		}

		writer.WriteHeader(http.StatusNotFound)
		_, _ = writer.Write([]byte("Not found"))
	}
}
