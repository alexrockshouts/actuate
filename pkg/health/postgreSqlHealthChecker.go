package health

import (
	"database/sql"
)

type PostgreSQLHealthChecker struct {
	DB *sql.DB
}

func (checker *PostgreSQLHealthChecker) CheckHealth() HealthCheckResult {
	var resp = HealthCheckResult{}
	var serviceName = "postgres"

	var version string
	err := checker.DB.QueryRow("select version()").Scan(&version)
	if err != nil {
		resp.Status = DOWN
		resp.Service = serviceName
		resp.Details = err
		return resp
	}

	resp.Status = UP
	resp.Service = serviceName
	resp.Details = version
	return resp
}
